import pygame
import math

pygame.init()

# sets the constant values for height and width
WIDTH, HEIGHT = 800, 800

# creates the display window
WIN = pygame.display.set_mode((WIDTH, HEIGHT))
# window caption
pygame.display.set_caption('Planet Simulations')

SUN_COLOR = (248, 255, 137)
EARTH_COLOR = (89, 126, 66)
MARS_COLOR = (171, 110, 82)
MERCURY_COLOR = (139, 133, 135)
VENUS_COLOR = (216, 171, 104)
SPACE_COLOR = (2, 2, 43)

class Planet:
    
    AU = 149.6e6 * 1000 # approx distance from earth to the sun
    G = 6.67428e-11 # gravitational constant
    SCALE = 225 / AU # 1AU astronomical scale is about 100 pixels in the window
    TIMESTEP = 3600*24 # 3 days
    
    def __init__(self, x, y, radius, color, mass):
        self.x = x # meters away from the sun
        self.y = y
        self.radius = radius
        self.color = color
        self.mass = mass
        
        self.orbit = [] # keep track of all the points the planet has travelled along
        self.isSun = False # tells us if the current planet is the sun
        self.distance_to_sun = 0
        
        # horizontal and vertical velocity
        self.x_vel = 0
        self.y_vel = 0
        
    def draw(self, win):
        x = self.x * self.SCALE + WIDTH / 2 # to put the planet in center screen
        y = self.y * self.SCALE + HEIGHT / 2
        pygame.draw.circle(win, self.color, (x,y), self.radius)
        
    def attraction(self, other):
        other_x, other_y = other.x, other.y
        distance_x = other_x - self.x
        distance_y = other_y - self.y
        #* distance between the two objects
        distance = math.sqrt(distance_x**2 + distance_y**2) # pythagorem theorem
        
        if other.isSun: # if the other object is the sun
            self.distance_to_sun = distance
            
        force = self.G * self.mass * other.mass / distance**2 # F = G * (m1*m2)/r^2
        theta = math.atan2(distance_y, distance_x) # gives us the angle theta
        #* calculate the x and y portions of the force
        force_x = math.cos(theta) * force
        force_y = math.sin(theta) * force
        
        return force_x, force_y
    
    def update_position(self, planets):
        total_fx = total_fy = 0
        
        for planet in planets:
            if self == planet: # if the planet is itself, skip it
                continue
            
            #* calculate the force that the other planet is putting on THIS planet
            fx, fy = self.attraction(planet)
            total_fx += fx
            total_fy += fy
            
        self.x_vel += total_fx / self.mass * self.TIMESTEP # acceleration = Force / mass
        self.y_vel += total_fy / self.mass * self.TIMESTEP # acceleration = Force / mass
        
        #* update x and y position using those calculated forces
        self.x += self.x_vel * self.TIMESTEP 
        self.y += self.y_vel * self.TIMESTEP
        self.orbit.append((self.x, self.y))
        

def main():
    
    run = True
    clock = pygame.time.Clock() #* like time.deltaTime in Unity
    
    sun = Planet(0,0, 30, SUN_COLOR, 1.98892 * 10**30)
    sun.isSun = True # because the sun is the sun, duh
    
    #! Planets initialization
    mercury = Planet(-0.387 * Planet.AU, 0, 8, MERCURY_COLOR, 0.330 * 10**24)
    mercury.y_vel = 47.4 * 1000
    venus = Planet(-0.723 * Planet.AU, 0, 14, VENUS_COLOR, 4.865 * 10**24)
    venus.y_vel = 35.01 * 1000
    earth = Planet(-1 * Planet.AU, 0, 16, EARTH_COLOR, 5.9742 * 10**24)
    earth.y_vel = 29.783 * 1000 # earth starting velocity
    mars = Planet(-1.524 * Planet.AU, 0, 12, MARS_COLOR, 6.39 * 10**23)
    mars.y_vel = 24.077 * 1000
    
    planets = [sun, earth, mars, mercury, venus]
    
    # the game will run as long as the player doesn't quit
    while run:
        
        clock.tick(60) #* max of times it can update per second
        WIN.fill(SPACE_COLOR) #* fills the background with white
        
        
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                
        for planet in planets: # displays all the planets onto the window
            planet.update_position(planets)
            planet.draw(WIN)
            
        pygame.display.update() #* updates the display
                
    pygame.quit()
    
main()